import { Component, OnInit } from '@angular/core';
import {DataBooksService} from "./data-books.service";
export interface Book {
  id: number;
  titolo :string;
  anno_pubb : number;
  isbn : number;
  trama : string;
  copertina : string;
  copie : number;
  argomento : string;
  nome : string;
}

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})


export class BooksComponent implements OnInit {
  public books: any;
  public selectedBook: Book;
  public isDetails: boolean;

  panelOpenState = false;
  constructor(private dataService: DataBooksService) { }

  ngOnInit(): void {
    this.dataService.sendGetRequest().subscribe((data: any[]) => {
      this.books = data;
    });
  }
  detailsShow(book: Book ): void {
    if (this.isDetails) {
      this.isDetails = false;
    } else {
      this.isDetails = true;
      this.selectedBook = book;
    }
  }

}
